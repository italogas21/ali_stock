package com.italogas.ali_stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliStockApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliStockApplication.class, args);
    }

}
